/**
 * Created by dgerk on 26.06.2017.
 */
$(document).ready(function() {
    $('#interForm').bootstrapValidator({
        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            login: {
                message: 'The username is not valid',
                validators: {
                    notEmpty: {
                        message: 'Логин не может быть пустым'
                    },
                    stringLength: {
                        min: 6,
                        max: 30,
                        message: 'Логин должен быть больше 6 и меньше чем 20 символов'
                    },
                    regexp: {
                        regexp: /^[a-zA-Z0-9_]+$/,
                        message: 'Могут быть использованы только a-z, A-Z, 0-9 символы'
                    }
                }
            },
            password: {
                validators: {
                    notEmpty: {
                        message: 'Пароль не может быть пустым'
                    }
                }
            }
        }
    });


    $('#registrationForm').bootstrapValidator({
        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            login: {
                message: 'Неверный логин',
                validators: {
                    notEmpty: {
                        message: 'Логин не может быть пустым'
                    },
                    stringLength: {
                        min: 6,
                        max: 30,
                        message: 'Логин должен быть больше 6 и меньше чем 20 символов'
                    },
                    regexp: {
                        regexp: /^[a-zA-Z0-9_]+$/,
                        message: 'Могут быть использованы только a-z, A-Z, 0-9 символы'
                    }
                }
            },
            password: {
                validators: {
                    notEmpty: {
                        message: 'Пароль не может быть пустым'
                    }
                }
            },
            email: {
                validators: {
                    notEmpty: {
                        message: 'Необходимо указать адрес электронной почты'
                    },
                    emailAddress: {
                        message: 'Неверный адрес электронной почты'
                    },
                    identical: {
                        field: 'confirmEmail',
                        message: 'Электронная почта и ее подтверждение не совпадают'
                    }
                }
            },
            confirmEmail: {
                validators: {
                    notEmpty: {
                        message: 'Необходимо указать адрес электронной почты'
                    },
                    identical: {
                        field: 'email',
                        message: 'Электронная почта и ее подтверждение не совпадают'
                    }
                }
            },
            name: {
                validators: {
                    notEmpty: {
                        message: 'Введите имя'
                    }
                }
            },
            lastName: {
                validators: {
                    notEmpty: {
                        message: 'Введите фамилию'
                    }
                }
            },
            city: {
                validators: {
                    notEmpty: {
                        message: 'Введите город'
                    }
                }
            },
            country: {
                validators: {
                    notEmpty: {
                        message: 'Введите город'
                    }
                }
            }
            // confirm: {
            //     validators: {
            //         notEmpty: {
            //             message: 'Please specify at least one language you can speak'
            //         }
            //     }
            // },
        }
    });

});