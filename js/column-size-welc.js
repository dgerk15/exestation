/**
 * Created by dgerk on 01.07.2017.
 */
jQuery(function($){
    var max_col_height = 0; // максимальная высота, первоначально 0
    $('.column').each(function(){ // цикл "для каждой из колонок"
        if ($(this).height() > max_col_height) { // если высота колонки больше значения максимальной высоты,
            max_col_height = $(this).height(); // то она сама становится новой максимальной высотой
        }
    });
    $('.column').height(max_col_height); // устанавливаем высоту каждой колонки равной значению максимальной высоты
});